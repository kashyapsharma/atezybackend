const express= require('express');
const MongoClient=require('mongodb').MongoClient;
const bodyParser =require('body-parser');
const db= require('./config/db');


const app= express();

var port=process.env.PORT || 8000;
var server =http.Server(app);

app.use(bodyParser.urlencoded({extended: true}))



MongoClient.connect(db.url, (err,client)=>{
    
    if(err) return console.log(err)
    const database = client.db('atezy');
    require('./app/routes')(app,database);
    server.listen(port,()=>{
         console.log("we are live on port "+port);
    })
})


// MongoClient.connect(url, function(err, client) {
//     assert.equal(null, err);
//     console.log("Connected successfully to server");
  
//     const db = client.db(dbName);
  
//     client.close();
//   });